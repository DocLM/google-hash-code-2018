#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "cli.h"

#define DEFAULT_PRINT_DATASET false
#define DEFAULT_INPUT_FILES_NUMBER 1
#define DEFAULT_INPUT_FILE_NAME "dataset.in"
#define DEFAULT_OUTPUT_FILE_NAME NULL
#define DEFAULT_BAN_PROBABILITY 0.1
#define DEFAULT_BAN_TIME 0
#define DEFAULT_SEED time(NULL)
#define DEFAULT_VERBOSITY 0
#define DEFAULT_ITERATIONS 1
#define DEFAULT_CONSTRUCTIVE_HEURISTIC "rides"
#define DEFAULT_RIDES_SORT "none"
#define DEFAULT_GLOBAL_BAN false

static const char *optString = "po:s:b:i:t:c:r:gvh?";

static const struct option longOpts[] = {
        { "print-dataset", no_argument, NULL, 'p' },
        { "output", required_argument, NULL, 'o' },
        { "seed", required_argument, NULL, 's' },
        { "ban-probability", required_argument, NULL, 'b' },
        { "iterations", required_argument, NULL, 'i' },
        { "ban-time", required_argument, NULL, 't' },
        { "constructive-heuristic", required_argument, NULL, 'c' },
        { "rides-sort", required_argument, NULL, 'r' },
        { "global-ban", no_argument, NULL, 'g' },
        { "verbose", no_argument, NULL, 'v' },
        { "help", no_argument, NULL, 'h' },
        { NULL, no_argument, NULL, 0 }
};

void display_usage() {
    printf( "Usage: gh2018 [options] <input>\n" );
    printf( "Options:\n" );
    printf( "  -p, --print-dataset                           Print information about <input> dataset\n" );
    printf( "  -o, --output <file>                           Write results to <file>.\n" );
    printf( "  -s, --seed <seed>                             Integer random seed.\n" );
    printf( "  -i, --iterations <iterations>                 Number of ART iterations.\n" );
    printf( "  -t, --ban-time <time>                         Number of ART iteration that a ride must wait when banned.\n" );
    printf( "  -b, --ban-probability <probability>           ART ban probability.\n" );
    printf( "  -c, --constructive-heuristic [rides|vehicles] Constructive heuristic used to build solutions.\n" );
    printf( "  -r, --rides-sort [start|short|long|none]      Rides sort strategy.\n" );
    printf( "  -g, --global-ban                              ART ban rides globally.\n" );
    printf( "  -v, --verbose                                 Use verbose output.\n" );
    printf( "  -h, -?, --help                                Print this message and exit.\n" );
    exit( EXIT_FAILURE );
}

global_args_t parse_command_line(int argc, char **argv) {
    global_args_t args;
    int opt, longIndex;

    //default values
    args.print_dataset          = DEFAULT_PRINT_DATASET;
    args.input_files_number     = DEFAULT_INPUT_FILES_NUMBER;
    args.input_file_name        = DEFAULT_INPUT_FILE_NAME;
    args.output_file_name       = DEFAULT_OUTPUT_FILE_NAME;
    args.ban_probability        = DEFAULT_BAN_PROBABILITY;
    args.ban_time               = DEFAULT_BAN_TIME;
    args.iterations             = DEFAULT_ITERATIONS;
    args.constructive_heuristic = DEFAULT_CONSTRUCTIVE_HEURISTIC;
    args.sort_criteria          = DEFAULT_RIDES_SORT;
    args.global_ban             = DEFAULT_GLOBAL_BAN;
    args.verbosity              = DEFAULT_VERBOSITY;
    args.seed                   = DEFAULT_SEED;

    do {
        opt = getopt_long(argc, argv, optString, longOpts, &longIndex);
        switch( opt ) {
            case 'p':
                args.print_dataset = true;
                break;

            case 'o':
                args.output_file_name = optarg;
                break;

            case 's':
                args.seed = strtoul(optarg, NULL, 0);
                break;

            case 'b':
                args.ban_probability = strtod(optarg, NULL);
                break;

            case 'i':
                args.iterations = strtoull(optarg, NULL, 0);
                break;

            case 't':
                args.ban_time = strtoull(optarg, NULL, 0);
                break;

            case 'c':
                args.constructive_heuristic = optarg;
                break;

            case 'r':
                args.sort_criteria = optarg;
                break;

            case 'g':
                args.global_ban = true;
                break;

            case 'v':
                args.verbosity++;
                break;

            case 'h':   /* print usage */
            case '?':
                display_usage();
                break;

            case 0:     /* long option without a short arg */
            default:
                break;
        }
    }
    while(opt != -1);

    if (argc - optind > 0)
        args.input_file_name = argv[optind];

    return args;
}
