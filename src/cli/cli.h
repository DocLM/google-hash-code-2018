//
// Created by Leonardo Medici on 2019-01-02.
//

#ifndef GOOGLE_HASH_CODE_2018_CLI_H
#define GOOGLE_HASH_CODE_2018_CLI_H

#include <stdbool.h>

typedef struct global_args_s {
    bool print_dataset;            /* -p option */
    const char *output_file_name;  /* -o option */
    int verbosity;                 /* -v option */
    unsigned int seed;             /* -s option */
    double ban_probability;        /* -b option */
    unsigned long long iterations; /* -i option */
    unsigned long long ban_time;   /* -t option */
    char *constructive_heuristic;  /* -c option */
    char *sort_criteria;           /* -r option */
    bool global_ban;               /* -g option */
    char *input_file_name;         /* input files */
    int input_files_number;        /* # of input files */
} global_args_t;

global_args_t parse_command_line(int argc, char **argv);
void display_usage();

#endif //GOOGLE_HASH_CODE_2018_CLI_H
