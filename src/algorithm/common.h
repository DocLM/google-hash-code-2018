//
// Created by Leonardo Medici on 2019-02-25.
//

#include <stdbool.h>

#include "solution.h"

#ifndef GOOGLE_HASH_CODE_2018_COMMON_H
#define GOOGLE_HASH_CODE_2018_COMMON_H

bool is_banned(rides_number_t ride, solution_t *solution);
bool free_ride(rides_number_t ride, solution_t *solution);
void ban_rides_global(solution_t *solution, double ban_probability);
void ban_rides_solution(solution_t *solution, double ban_probability);
void assign_banned_rides(solution_t *solution);

#endif //GOOGLE_HASH_CODE_2018_COMMON_H
