//
// Created by Leonardo Medici on 2019-02-27.
//

#ifndef GOOGLE_HASH_CODE_2018_FILL_VEHICLES_H
#define GOOGLE_HASH_CODE_2018_FILL_VEHICLES_H

#include "solution.h"

void fill_vehicles(solution_t *solution);

#endif //GOOGLE_HASH_CODE_2018_FILL_VEHICLES_H
