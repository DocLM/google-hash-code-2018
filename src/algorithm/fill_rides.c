//
// Created by Leonardo Medici on 2019-02-27.
//

#include <assert.h>
#include <syslog.h>

#include "common.h"
#include "fill_rides.h"

void fill_rides(solution_t *solution)
{
    assert(solution && "Invalid solution pointer");
    assert(solution->dataset && "Invalid solution dataset pointer");

    dataset_t *dataset = solution->dataset;
    for (rides_number_t r = 0; r < dataset->rides_number; r++)
    {
        score_t score = 0;
        search_insertion_result_t search_result = {.found = false};

        if (free_ride(r, solution))
        {
            for (vehicles_number_t v = 0; v < dataset->vehicles_number; v++)
            {
                search_insertion_result_t result = solution_search_insertion(v, r, solution);
                if (result.found) {
                    ride_path_t *ride = &solution->paths.rides[r];

                    score_t assignment_score = ride->ride->earliest_start >= result.start_time ? ride->length + solution->dataset->bonus : ride->length;

                    if (assignment_score > score) {
                        score = assignment_score;
                        search_result = result;
                    }
                }
            }
        }

        if (search_result.found)
        {
            solution_add_ride_to_vehicle(search_result.vehicle, search_result.ride, r, search_result.start_time, solution);
            syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] partial solution score: %"PRIscore, solution->solution_iteration, solution->score);
            continue;
        }

        syslog(LOG_INFO, "Iteration[%"PRIart_iteration"] ride %"PRIrides_number" not assigned", solution->solution_iteration, r);
    }
}