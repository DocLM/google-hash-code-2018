//
// Created by Leonardo Medici on 2019-01-09.
//

#ifndef GOOGLE_HASH_CODE_2018_ALGORITHM_H
#define GOOGLE_HASH_CODE_2018_ALGORITHM_H

#include "solution.h"

#include "fill_rides.h"
#include "fill_vehicles.h"

typedef void (*constructive_heuristic_t)(solution_t *solution);
typedef void (*ban_criteria_t)(solution_t *solution, double ban_probability);

typedef struct ART_config_s {
    dataset_t *dataset;
    art_iteration_t iterations;
    art_iteration_t ban_time;
    double          ban_probability;
    constructive_heuristic_t constructive_heuristic;
    rides_sort_criteria_t    sort_criteria;
    ban_criteria_t ban_criteria;
} ART_config_t;

constructive_heuristic_t parse_heuristic(char *name);
solution_t *ART_run(unsigned int seed, ART_config_t configuration);

#endif //GOOGLE_HASH_CODE_2018_ALGORITHM_H
