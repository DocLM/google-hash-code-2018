//
// Created by Leonardo Medici on 2019-01-09.
//
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/param.h>

#include "algorithm.h"
#include "common.h"
#include "solution.h"

typedef enum CONSTRUCTIVE_HEURISTICS_e {
    FILL_RIDES,
    FILL_VEHICLES,
    INVALID
} CONSTRUCTIVE_HEURISTICS_t;

const constructive_heuristic_t constructive_heuristics[] = { fill_rides, fill_vehicles, NULL };
const char *heuristics_name[] = { "rides", "vehicles", NULL };

constructive_heuristic_t parse_heuristic (char *name)
{
    if(strcmp(heuristics_name[FILL_RIDES], name) == 0)
        return constructive_heuristics[FILL_RIDES];

    if(strcmp(heuristics_name[FILL_VEHICLES], name) == 0)
        return constructive_heuristics[FILL_VEHICLES];

    return constructive_heuristics[INVALID];
}

solution_t *ART_run(unsigned int seed, ART_config_t configuration)
{
    solution_t *solution = solution_alloc(configuration.dataset);
    solution_t *optimum  = solution_alloc(configuration.dataset);

    srand(seed);

    art_iteration_t iterations = MIN(ART_ITERATIONS_MAX - configuration.ban_time, configuration.iterations);

    solution_initialize(solution, configuration.sort_criteria);
    solution_clone(solution, optimum);

    for (art_iteration_t i = 0; i < iterations; i++)
    {
        solution_reset(solution, false);

        solution->iterations         = iterations;
        solution->solution_iteration = i;
        solution->ban_time           = configuration.ban_time;

        configuration.constructive_heuristic(solution);

        assign_banned_rides(solution);

        if(solution->score > optimum->score)
            solution_clone(solution, optimum);

        configuration.ban_criteria(solution, configuration.ban_probability);
    }

    solution_free(solution);

    syslog(LOG_NOTICE, "Best solution score: %"PRIscore, optimum->score);
    return optimum;
}
