//
// Created by Leonardo Medici on 2019-02-12.
//

#ifndef GOOGLE_HASH_CODE_2018_SOLUTION_H
#define GOOGLE_HASH_CODE_2018_SOLUTION_H

#include <stdbool.h>
#include <stdlib.h>
#include "../dataset/dataset.h"

typedef uint64_t art_iteration_t;
typedef uint64_t score_t;

#define PRIart_iteration PRIu64
#define PRIscore PRIu64

#define ART_ITERATIONS_MAX UINT64_MAX

typedef struct ride_path_s {
    ride_t *ride; /* pointer to dataset ride */
    bool   free;  /* allow ride to be assigned */
    rides_number_t    number;      /* ride input file entry number */
    rides_number_t    predecessor; /* index of ride predecessor */
    rides_number_t    successor;   /* index of ride successor */
    map_distance_t    length;      /* cached distance between ride start and ride end */
    score_t           score;       /* sum of path score before the ride  */
    art_iteration_t   ban_time;    /* ART iteration that ban the ride */
    iteration_time_t  start_time;  /* time when the vehicle is at ride start */
    iteration_time_t  minimum_recompute_time; /* the minimum time units before the ride chain is invalidated */
} ride_path_t;

typedef struct vehicle_path_s {
    rides_number_t   start; /* index of the first ride on the path */
    rides_number_t   end;   /* index of the last ride on the path */
    score_t          score; /* total score of the path */
} vehicle_path_t;

typedef struct paths_s {
    vehicle_path_t *vehicles; /* array of vehicles */
    ride_path_t    *rides;    /* array of rides */
} paths_t;

typedef struct solution_s {
    dataset_t *dataset; /* reference dataset */
    art_iteration_t  iterations;         /* number of ART iterations */
    art_iteration_t  solution_iteration; /* ART iteration that generate the solution */
    art_iteration_t  ban_time;           /* length (in ART iteration unit) of the ban */
    score_t   score; /* total score of the solution */
    paths_t   paths; /* paths data structure */
} solution_t;

typedef struct search_insertion_result_s {
    bool found;
    vehicles_number_t vehicle;
    rides_number_t    ride;
    iteration_time_t  start_time;
} search_insertion_result_t;

typedef int (*rides_sort_criteria_t)(const ride_path_t *a, const ride_path_t *b);

solution_t *solution_alloc(dataset_t *dataset);
void solution_initialize(solution_t *solution, rides_sort_criteria_t sort_criteria);
void solution_reset(solution_t *solution, bool reset_ban);
void solution_clone(solution_t *origin, solution_t *target);
void solution_free(solution_t *solution);
void solution_print(FILE *file, solution_t *solution);

search_insertion_result_t solution_search_insertion(vehicles_number_t vehicle_number, rides_number_t ride_number, solution_t *solution);
void solution_add_ride_to_vehicle(vehicles_number_t  vehicle, rides_number_t insertion_point, rides_number_t ride, iteration_time_t start_time, solution_t *solution);

rides_sort_criteria_t parse_sort_criteria(char *name);

#endif //GOOGLE_HASH_CODE_2018_SOLUTION_H
