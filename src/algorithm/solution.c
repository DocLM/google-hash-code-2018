//
// Created by Leonardo Medici on 2019-02-12.
//

#include <assert.h>
#include <string.h>
#include <syslog.h>
#include <sys/param.h>

#include "common.h"
#include "solution.h"

#define DEFAULT_BAN_TIME   0
#define DEFAULT_ITERATIONS 0
#define DEFAULT_SOLUTION_ITERATION 0
#define DEFAULT_RIDE_BAN_TIME 0

ride_t zero = { .start = 0, .end = 0, .earliest_start = 0, .latest_finish = 0 };

typedef int (* compar)(const void *, const void *);
int compare_ride_start_time(const ride_path_t *first, const ride_path_t *second);
int compare_ride_shortest(const ride_path_t *first, const ride_path_t *second);
int compare_ride_longest(const ride_path_t *first, const ride_path_t *second);

typedef enum SORT_CRITERIA_e {
    START_TIME,
    SHORTEST_RIDE,
    LONGEST_RIDE,
    NONE
} SORT_CRITERIA_t;

rides_sort_criteria_t sort_criteria[] = { compare_ride_start_time, compare_ride_shortest, compare_ride_longest, NULL };
const char *sort_criteria_name[] = { "start", "short", "long", "none" };

rides_sort_criteria_t parse_sort_criteria(char *name)
{
    if(strcmp(sort_criteria_name[START_TIME], name) == 0)
        return sort_criteria[START_TIME];

    if(strcmp(sort_criteria_name[SHORTEST_RIDE], name) == 0)
        return sort_criteria[SHORTEST_RIDE];

    if(strcmp(sort_criteria_name[LONGEST_RIDE], name) == 0)
        return sort_criteria[LONGEST_RIDE];

    return sort_criteria[NONE];
}

solution_t *solution_alloc(dataset_t *dataset)
{
    assert(dataset && "Invalid dataset pointer");

    solution_t *solution = calloc(1, sizeof(solution_t));

    if(solution != NULL)
    {
        solution->dataset = dataset;

        paths_t paths;
        paths.vehicles  = calloc(dataset->vehicles_number, sizeof(vehicle_path_t));
        paths.rides     = calloc(dataset->rides_number + dataset->vehicles_number, sizeof(ride_path_t));
        solution->paths = paths;

        if(paths.rides && paths.vehicles)
        {
            syslog(LOG_DEBUG, "Allocated empty solution");
            return solution;
        }

        syslog(LOG_DEBUG, "Solution allocated partially [vehicles: %d, rides: %d]", paths.vehicles != NULL, paths.rides != NULL);

        solution_free(solution);
        free(solution);
    }

    syslog(LOG_DEBUG, "Failed to allocate solution");
    return NULL;
}

void solution_initialize(solution_t *solution, rides_sort_criteria_t sort_criteria)
{
    assert(solution && "invalid solution pointer");
    assert(solution->dataset && "Invalid dataset pointer");
    assert(solution->paths.rides    && "Invalid solution paths rides pointer");
    assert(solution->paths.vehicles && "Invalid solution paths vehicles pointer");

    solution->ban_time           = DEFAULT_BAN_TIME;
    solution->iterations         = DEFAULT_ITERATIONS;
    solution->solution_iteration = DEFAULT_SOLUTION_ITERATION;
    solution->score   = 0;

    for(vehicles_number_t v = 0; v < solution->dataset->vehicles_number; v++)
    {
        rides_number_t ride_mock = solution->dataset->rides_number + v;

        solution->paths.vehicles[v].start  = ride_mock;
        solution->paths.vehicles[v].end    = ride_mock;
        solution->paths.vehicles[v].score  = 0;

        solution->paths.rides[ride_mock].ride     = &zero;
        solution->paths.rides[ride_mock].free     = false;
        solution->paths.rides[ride_mock].score    = 0;
        solution->paths.rides[ride_mock].length   = 0;
        solution->paths.rides[ride_mock].successor   = ride_mock;
        solution->paths.rides[ride_mock].predecessor = ride_mock;
        solution->paths.rides[ride_mock].start_time  = 0;
        solution->paths.rides[ride_mock].minimum_recompute_time = 0;
    }

    for (rides_number_t r = 0; r < solution->dataset->rides_number; r++)
    {
        ride_path_t *ride = &solution->paths.rides[r];
        ride->ban_time = DEFAULT_RIDE_BAN_TIME;
        ride->free     = true;
        ride->number   = r;
        ride->ride     = &solution->dataset->rides[r];
        ride->score    = 0;
        ride->length   = solution->dataset->distance(solution->dataset->rides[r].start, solution->dataset->rides[r].end);
        ride->start_time = 0;
        ride->minimum_recompute_time = 0;
    }

    if(sort_criteria)
        qsort(solution->paths.rides, solution->dataset->rides_number, sizeof(ride_path_t), (compar) sort_criteria);

    for (rides_number_t r = 0; r < solution->dataset->rides_number; r++)
    {
        ride_path_t *ride = &solution->paths.rides[r];
        ride->predecessor = r;
        ride->successor   = r;
        syslog(LOG_DEBUG,"Initialized ride %"PRIrides_number" [number: %"PRIrides_number", predecessor: %"PRIrides_number", successor: %"PRIrides_number", ban_time: %"PRIart_iteration", free: %u, ride: %p, score: %"PRIscore", length: %"PRImap_distance", minimum_recompute_time: %"PRIiteration_time"]", r, ride->number, ride->predecessor, ride->successor, ride->ban_time, ride->free, ride->ride, ride->score, ride->length, ride->minimum_recompute_time);
    }

    syslog(LOG_DEBUG, "Initialized solution");
}


void solution_reset(solution_t *solution, bool reset_ban)
{
    assert(solution && "Invalid solution pointer");
    assert(solution->dataset && "Invalid dataset pointer");
    assert(solution->paths.rides    && "Invalid solution paths rides pointer");
    assert(solution->paths.vehicles && "Invalid solution paths vehicles pointer");

    solution->score = 0;

    for (rides_number_t i = 0; i < solution->dataset->rides_number; i++)
    {
        if(reset_ban)
            solution->paths.rides[i].ban_time = DEFAULT_RIDE_BAN_TIME;

        solution->paths.rides[i].free     = true;
        solution->paths.rides[i].score    = 0;
        solution->paths.rides[i].successor   = i;
        solution->paths.rides[i].predecessor = i;
        solution->paths.rides[i].start_time  = 0;
        solution->paths.rides[i].minimum_recompute_time = 0;
    }

    for (vehicles_number_t i = 0; i < solution->dataset->vehicles_number; i++)
    {
        rides_number_t ride_mock = solution->dataset->rides_number + i;
        solution->paths.vehicles[i].start = ride_mock;
        solution->paths.vehicles[i].end   = solution->dataset->rides_number + i;
        solution->paths.vehicles[i].score = 0;

        solution->paths.rides[ride_mock].free     = false;
        solution->paths.rides[ride_mock].score    = 0;
        solution->paths.rides[ride_mock].length   = 0;
        solution->paths.rides[ride_mock].successor   = ride_mock;
        solution->paths.rides[ride_mock].predecessor = ride_mock;
        solution->paths.rides[ride_mock].start_time  = 0;
        solution->paths.rides[ride_mock].minimum_recompute_time = 0;
    }
}

void solution_free(solution_t *solution)
{
    assert(solution && "Invalid solution pointer");

    if(solution->paths.rides)
    {
        free(solution->paths.rides);
        solution->paths.rides = NULL;
    }

    if(solution->paths.vehicles)
    {
        free(solution->paths.vehicles);
        solution->paths.vehicles = NULL;
    }
}

void solution_print(FILE *output, solution_t *solution)
{
    assert(output   && "Invalid output file");
    assert(solution && "Invalid solution pointer");

    for (vehicles_number_t i = 0; i < solution->dataset->vehicles_number; i++) {

            rides_number_t count   = 0;
            rides_number_t current = solution->paths.vehicles[i].start;
            rides_number_t last    = solution->paths.vehicles[i].start;
            do
            {
                count++;
                last    = current;
                current = solution->paths.rides[current].successor;
            }
            while (last != current);

            count -= 1;

            fprintf(output, "%"PRIrides_number, count);

            current = solution->paths.rides[solution->paths.vehicles[i].start].successor;

            for (rides_number_t j = 0; j < count; j++) {
                fprintf(output, " %"PRIrides_number, solution->paths.rides[current].number);
                current = solution->paths.rides[current].successor;
            }

            fprintf(output, "\n");
    }
}

void solution_clone(solution_t *origin, solution_t *destination)
{
    assert(origin      && "Invalid origin solution pointer");
    assert(destination && "Invalid destination solution pointer");

    destination->score = origin->score;
    destination->solution_iteration = origin->solution_iteration;
    destination->ban_time   = origin->ban_time;
    destination->iterations = origin->iterations;

    for (rides_number_t r = 0; r < origin->dataset->rides_number + origin->dataset->vehicles_number; r++)
        destination->paths.rides[r] = origin->paths.rides[r];

    for (vehicles_number_t v = 0; v < origin->dataset->vehicles_number ; v++)
        destination->paths.vehicles[v] = origin->paths.vehicles[v];
}

void adjust_recomputation_time(rides_number_t ride, solution_t *solution)
{
    ride_path_t *ride_path = &solution->paths.rides[ride];
    bool waiting = ride_path->start_time <= ride_path->ride->earliest_start;
    if(!waiting && ride_path->successor != ride && solution->paths.rides[ride_path->successor].minimum_recompute_time < ride_path->minimum_recompute_time)
        ride_path->minimum_recompute_time = solution->paths.rides[ride_path->successor].minimum_recompute_time;

    rides_number_t current = ride;
    rides_number_t predecessor = ride_path->predecessor;

    waiting = solution->paths.rides[predecessor].start_time <= solution->paths.rides[predecessor].ride->earliest_start;
    while(current != predecessor && !waiting && solution->paths.rides[current].minimum_recompute_time < solution->paths.rides[predecessor].minimum_recompute_time)
    {
        solution->paths.rides[predecessor].minimum_recompute_time = solution->paths.rides[current].minimum_recompute_time;
        current     = predecessor;
        predecessor = solution->paths.rides[predecessor].predecessor;

        waiting = solution->paths.rides[predecessor].start_time <= solution->paths.rides[predecessor].ride->earliest_start;
    }
}

search_insertion_result_t solution_search_insertion(vehicles_number_t vehicle_number, rides_number_t ride_number, solution_t *solution)
{
    search_insertion_result_t result = { .vehicle = vehicle_number, .found = false };
    vehicle_path_t *vehicle   = &solution->paths.vehicles[vehicle_number];
    ride_path_t    *ride_path = &solution->paths.rides[ride_number];

    rides_number_t vehicle_ride_number = vehicle->end;
    ride_path_t    *vehicle_ride       = NULL;
    iteration_time_t first_available_time = 0;
    bool can_finish_in_time               = false;

    do {
        syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] evaluating ride %"PRIrides_number" after %"PRIrides_number" for vehicle %"PRIvehicle_number, solution->solution_iteration, ride_number, vehicle_ride_number, vehicle_number);

        vehicle_ride = &solution->paths.rides[vehicle_ride_number];

        first_available_time = MAX(vehicle_ride->start_time, vehicle_ride->ride->earliest_start) + vehicle_ride->length;

        map_distance_t distance = solution->dataset->distance(vehicle_ride->ride->end, ride_path->ride->start);

        iteration_time_t start_time   = first_available_time + distance;
        can_finish_in_time = start_time + ride_path->length <= ride_path->ride->latest_finish;

        if(can_finish_in_time)
        {
            if(vehicle_ride_number == vehicle_ride->successor)
            {
                result.found      = true;
                result.start_time = start_time;
                result.ride       = vehicle_ride_number;

                syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] found a valid position for ride %"PRIrides_number" after %"PRIrides_number, solution->solution_iteration, ride_number, vehicle_ride_number);
                return result;
            }

            ride_path_t *successor            = &solution->paths.rides[vehicle_ride->successor];
            map_distance_t successor_distance = solution->dataset->distance(ride_path->ride->end, successor->ride->start);

            iteration_time_t successor_start_time = MAX(start_time, ride_path->ride->earliest_start) + ride_path->length + successor_distance;

            assert(successor_start_time >= successor->start_time && "Add a ride can't go back in time");

            iteration_time_t time_delta = successor_start_time - successor->start_time;

            if(time_delta <= successor->minimum_recompute_time)
            {
                result.found = true;
                result.start_time = start_time;
                result.ride = vehicle_ride_number;

                syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] found a valid position for ride %"PRIrides_number" after %"PRIrides_number, solution->solution_iteration, ride_number, vehicle_ride_number);

                return result;
            }
        }

        vehicle_ride_number = vehicle_ride->predecessor;

    }
    while (vehicle_ride_number != vehicle->start && vehicle_ride->start_time >= ride_path->ride->earliest_start);

    return result;
}

void solution_add_ride_to_vehicle(vehicles_number_t  vehicle, rides_number_t insertion_point, rides_number_t ride, iteration_time_t start_time, solution_t *solution)
{
    ride_path_t *ride_path = &solution->paths.rides[ride];
    score_t assignment_score        = ride_path->ride->earliest_start >= start_time ? ride_path->length + solution->dataset->bonus : ride_path->length;
    iteration_time_t recompute_time = ride_path->ride->earliest_start >= start_time ? ride_path->ride->earliest_start - start_time : ride_path->ride->latest_finish - ride_path->length - start_time;

    ride_path->free = false;

    ride_path->start_time = start_time;
    solution->score += assignment_score;

    if(solution->paths.rides[insertion_point].successor == insertion_point)
    {
        solution->paths.rides[insertion_point].successor = ride;
        ride_path->predecessor = insertion_point;
        ride_path->successor   = ride;
        ride_path->minimum_recompute_time = recompute_time;
        ride_path->score = assignment_score;

        solution->paths.vehicles[vehicle].end = ride;
        solution->paths.vehicles[vehicle].score += assignment_score;

        syslog(LOG_INFO, "Iteration[%"PRIart_iteration"] assigned ride %"PRIrides_number" at the end of the queue of vehicle %"PRIvehicle_number, solution->solution_iteration, ride, vehicle);

        adjust_recomputation_time(ride, solution);

        return;
    }

    rides_number_t successor = solution->paths.rides[insertion_point].successor;

    solution->paths.rides[insertion_point].successor = ride;
    solution->paths.rides[successor].predecessor     = ride;

    solution->paths.rides[ride].predecessor = insertion_point;
    solution->paths.rides[ride].successor   = successor;

    ride_path->minimum_recompute_time = recompute_time;
    ride_path->score = assignment_score;

    solution->paths.vehicles[vehicle].score += assignment_score;

    iteration_time_t time_delta =
            MAX(solution->paths.rides[ride].start_time, solution->paths.rides[ride].ride->earliest_start)
            + solution->paths.rides[ride].length
            + solution->dataset->distance(solution->paths.rides[ride].ride->end, solution->paths.rides[successor].ride->start)
            - solution->paths.rides[successor].start_time;

    rides_number_t current = ride;
    successor = solution->paths.rides[current].successor;

    syslog(LOG_INFO, "Iteration[%"PRIart_iteration"] assigned ride %"PRIrides_number" after %u for vehicle %"PRIvehicle_number, solution->solution_iteration, ride, insertion_point, vehicle);

    bool waiting = false;
    while (current != successor && !waiting)
    {
        assert(MAX(solution->paths.rides[successor].start_time + time_delta, solution->paths.rides[successor].ride->earliest_start) + solution->paths.rides[successor].length <= solution->paths.rides[successor].ride->latest_finish);

        solution->paths.rides[successor].start_time += time_delta;
        waiting = solution->paths.rides[successor].start_time <= solution->paths.rides[successor].ride->earliest_start;

        assert(solution->paths.rides[successor].minimum_recompute_time >= time_delta && "Minimum recompute time constraint");
        solution->paths.rides[successor].minimum_recompute_time -= time_delta;

        current   = successor;
        successor = solution->paths.rides[successor].successor;
    }

    adjust_recomputation_time(ride, solution);
}

int compare_ride_start_time(const ride_path_t *first, const ride_path_t *second)
{
    assert(first  != NULL && "Invalid first element pointer");
    assert(second != NULL && "Invalid second element pointer");

    if(second->ride->earliest_start < first->ride->earliest_start)
        return -1;

    if(second->ride->earliest_start > first->ride->earliest_start)
        return 1;

    return 0;
}

int compare_ride_shortest(const ride_path_t *first, const ride_path_t *second)
{
    assert(first  != NULL && "Invalid first element pointer");
    assert(second != NULL && "Invalid second element pointer");

    if(first->length < second->length)
        return -1;

    if(first->length > second->length)
        return 1;

    return 0;
}

int compare_ride_longest(const ride_path_t *first, const ride_path_t *second)
{
    return compare_ride_shortest(second, first);
}
