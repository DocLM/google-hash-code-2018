//
// Created by Leonardo Medici on 2019-02-27.
//

#include <assert.h>
#include <syslog.h>

#include "common.h"
#include "fill_vehicles.h"

void fill_vehicles(solution_t *solution)
{
    assert(solution && "Invalid solution pointer");
    assert(solution->dataset && "Invalid solution dataset pointer");

    dataset_t *dataset = solution->dataset;

    for (vehicles_number_t v = 0; v < dataset->vehicles_number; v++)
    {
        for (rides_number_t r = 0; r < dataset->rides_number; r++)
        {
            if (free_ride(r, solution))
            {
                search_insertion_result_t result = solution_search_insertion(v, r, solution);
                if (result.found)
                {
                    solution_add_ride_to_vehicle(result.vehicle, result.ride, r, result.start_time, solution);
                    syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] partial solution score: %"PRIscore, solution->solution_iteration, solution->score);
                    continue;
                }

                syslog(LOG_INFO, "Iteration[%"PRIart_iteration"] ride %"PRIrides_number" not assigned to vehicle %"PRIvehicle_number, solution->solution_iteration, r, v);
            }
        }
    }
}
