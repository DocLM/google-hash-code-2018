//
// Created by Leonardo Medici on 2019-02-25.
//

#include <syslog.h>

#include "common.h"

double get_probability() {
    double probability = rand() / (double) RAND_MAX;
    syslog(LOG_DEBUG, "Extracted probability %f", probability);
    return probability;
}

bool is_banned(rides_number_t ride, solution_t *solution)
{
    if (solution->solution_iteration >= solution->paths.rides[ride].ban_time)
    {
        syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] Ride %"PRIrides_number" isn't banned", solution->solution_iteration, ride);
        return false;
    }

    syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] Ride %"PRIrides_number" was banned at iteration %"PRIart_iteration, solution->solution_iteration, ride, solution->paths.rides[ride].ban_time - solution->ban_time);
    return true;
}

bool free_ride(rides_number_t ride, solution_t *solution)
{
    if (solution->paths.rides[ride].free)
    {
        syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] Ride %"PRIrides_number" is free", solution->solution_iteration, ride);
        return !is_banned(ride, solution);
    }

    syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] Ride %"PRIrides_number" already picked", solution->solution_iteration, ride);
    return false;
}

void remove_rides_ban(solution_t *solution)
{
    bool all_banned = true;
    art_iteration_t min_ban_iteration = ART_ITERATIONS_MAX;
    for (rides_number_t ride = 0; ride < solution->dataset->rides_number && all_banned; ride++)
    {
        all_banned = is_banned(ride, solution);
        if(min_ban_iteration > solution->paths.rides[ride].ban_time)
            min_ban_iteration = solution->paths.rides[ride].ban_time;
    }

    if(all_banned)
    {
        for (rides_number_t ride = 0; ride < solution->dataset->rides_number; ride++)
        {
            if(solution->paths.rides[ride].ban_time == min_ban_iteration)
            {
                solution->paths.rides[ride].ban_time = 0;
                syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] ride %"PRIrides_number" unbanned before time ban constraint", solution->solution_iteration, ride);
            }
        }
    }
}

void ban_rides_global(solution_t *solution, double ban_probability)
{
    for(rides_number_t ride = 0; ride < solution->dataset->rides_number; ride++)
    {
        if(solution->solution_iteration >= solution->paths.rides[ride].ban_time && get_probability() <= ban_probability)
        {
            solution->paths.rides[ride].ban_time = solution->solution_iteration + solution->ban_time;
            syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] ban ride %"PRIrides_number, solution->solution_iteration, ride);
            return;
        }
    }

    remove_rides_ban(solution);
}

void ban_rides_solution(solution_t *solution, double ban_probability)
{
    for (int vehicle = 0; vehicle < solution->dataset->vehicles_number; vehicle++)
    {
        rides_number_t ride = solution->paths.vehicles[vehicle].start;
        while(ride != solution->paths.rides[ride].successor)
        {
            ride = solution->paths.rides[ride].successor;
            if(solution->solution_iteration >= solution->paths.rides[ride].ban_time && get_probability() <= ban_probability)
            {
                solution->paths.rides[ride].ban_time = solution->solution_iteration + solution->ban_time;
                syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] ban ride %"PRIrides_number, solution->solution_iteration, ride);
                return;
            }
        }
    }

    remove_rides_ban(solution);
}

void assign_banned_rides(solution_t *solution)
{
    for (rides_number_t r = 0; r < solution->dataset->rides_number; r++)
    {
        if(solution->paths.rides[r].free)
        {
            for (vehicles_number_t v = 0; v < solution->dataset->vehicles_number; v++)
            {
                if(is_banned(r, solution))
                {
                    search_insertion_result_t result = solution_search_insertion(v, r, solution);
                    if (result.found)
                    {
                        solution_add_ride_to_vehicle(result.vehicle, result.ride, r, result.start_time, solution);
                        syslog(LOG_DEBUG, "Iteration[%"PRIart_iteration"] banned ride %"PRIrides_number" assigned", solution->solution_iteration, r);
                        break;
                    }
                }
            }
        }
    }
}
