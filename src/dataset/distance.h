//
// Created by Leonardo Medici on 2019-01-03.
//

#ifndef GOOGLE_HASH_CODE_2018_DISTANCE_H
#define GOOGLE_HASH_CODE_2018_DISTANCE_H

#include "dataset.h"

map_distance_t manhattan(point_t origin, point_t destination);

#endif //GOOGLE_HASH_CODE_2018_DISTANCE_H
