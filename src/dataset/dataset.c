//
// Created by Leonardo Medici on 2019-01-03.
//

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <syslog.h>

#include "dataset.h"
#include "distance.h"

#define MAX_GRID     10000U
#define MAX_VEHICLES 10000U
#define MAX_RIDES    10000U
#define MAX_BONUS    10000U
#define MAX_STEPS    1000000000UL


int parse_header(FILE *file, dataset_t *dataset)
{
    int16_t rows, columns, vehicles_number, rides_number, bonus;
    int32_t steps;

    int items = fscanf(
            file,
            "%"SCNd16" %"SCNd16" %"SCNd16" %"SCNd16" %"SCNd16" %"SCNd32,
            &rows,
            &columns,
            &vehicles_number,
            &rides_number,
            &bonus,
            &steps
    );

    if (items == 6)
    {
        if (rows > 0 && rows <= MAX_GRID)
        {
            if (columns > 0 && columns <= MAX_GRID)
            {
                if (vehicles_number > 0 && vehicles_number <= MAX_VEHICLES)
                {
                    if(rides_number > 0 && rides_number <= MAX_RIDES)
                    {
                        if (bonus > 0 && bonus <= MAX_BONUS)
                        {
                            if (steps > 0 && steps <= MAX_STEPS)
                            {
                                dataset->rows    = (map_coordinate_t) rows;
                                dataset->columns = (map_coordinate_t) columns;
                                dataset->vehicles_number = (vehicles_number_t) vehicles_number;
                                dataset->rides_number    = (rides_number_t) rides_number;
                                dataset->bonus = (bonus_size_t) bonus;
                                dataset->steps = (iteration_time_t) steps;
                                return 0;
                            }

                            syslog(LOG_ERR, "Invalid number of steps: %"PRId32, steps);
                            return -1;
                        }

                        syslog(LOG_ERR, "Invalid rides bonus: %"PRId16, bonus);
                        return -1;
                    }

                    syslog(LOG_ERR, "Invalid rides number: %"PRId16, rides_number);
                    return -1;
                }

                syslog(LOG_ERR, "Invalid vehicles number: %"PRId16, vehicles_number);
                return -1;
            }
            syslog(LOG_ERR, "Invalid grid columns: %"PRId16, columns);
            return -1;
        }

        syslog(LOG_ERR, "Invalid grid rows: %"PRId16, rows);
        return -1;
    }

    syslog(LOG_ERR, "Invalid header format, parsed %d elements", items);
    return -1;
}

int parse_ride(FILE *file, ride_t *ride, dataset_t *dataset)
{
    int16_t start_row, start_column, end_row, end_column;
    int32_t earliest_start, latest_finish;

    point_t origin;
    point_t destination;

    int items = fscanf(
            file,
            "%"SCNd16" %"SCNd16" %"SCNd16" %"SCNd16" %"SCNd32" %"SCNd32,
            &start_row,
            &start_column,
            &end_row,
            &end_column,
            &earliest_start,
            &latest_finish
    );

    if (items == 6)
    {
        if(start_row >= 0 && start_row < dataset->rows)
        {
            if(start_column >= 0 && start_column < dataset->columns)
            {
                origin.x = (map_coordinate_t) start_row;
                origin.y = (map_coordinate_t) start_column;

                if(end_row >= 0 && end_row < dataset->rows)
                {
                    if(end_column >= 0 && end_column < dataset->columns)
                    {
                        destination.x = (map_coordinate_t) end_row;
                        destination.y = (map_coordinate_t) end_column;

                        map_coordinate_t distance = dataset->distance(origin, destination);
                        if (distance > 0)
                        {
                            if(earliest_start >= 0 && earliest_start < dataset->steps)
                            {
                                if (latest_finish >= 0 && latest_finish <= dataset->steps && latest_finish >= earliest_start + distance)
                                {
                                    ride->start = origin;
                                    ride->end   = destination;

                                    ride->earliest_start = (iteration_time_t) earliest_start;
                                    ride->latest_finish  = (iteration_time_t) latest_finish;

                                    return 0;
                                }

                                syslog(LOG_ERR, "%"PRId32" is not a valid finish step", latest_finish);
                                return -1;
                            }

                            syslog(LOG_ERR, "%"PRId32" is not a valid start step", earliest_start);
                            return -1;
                        }

                        syslog(LOG_ERR, "Distance between origin (%"PRId16", %"PRId16") and destination (%"PRId16", %"PRId16") must be greater than 0", origin.x, origin.y, destination.x, destination.y);
                        return -1;
                    }

                    syslog(LOG_ERR, "%"PRId16" is not a valid column", end_column);
                    return -1;
                }

                syslog(LOG_ERR, "%"PRId16" is not a valid row", end_row);
                return -1;
            }

            syslog(LOG_ERR, "%"PRId16" is not a valid column", start_column);
            return -1;
        }

        syslog(LOG_ERR, "%"PRId16" is not a valid row", start_row);
        return -1;
    }

    if(items >= 0)
    {
        syslog(LOG_ERR, "Invalid ride row, parsed %d elements", items);
        return -1;
    }

    syslog(LOG_ERR, "Unexpected end of file");
    return -1;
}

dataset_t dataset_load(char *path)
{
    dataset_t dataset;
    dataset.valid    = false;
    dataset.distance = manhattan;
    dataset.rows    = 0;
    dataset.columns = 0;
    dataset.vehicles_number = 0;
    dataset.rides_number    = 0;
    dataset.bonus = 0;
    dataset.steps = 0;
    dataset.rides = NULL;

    FILE *dataset_file = fopen(path, "r");

    if (dataset_file)
    {
        if (parse_header(dataset_file, &dataset) != -1)
        {
            ride_t *rides = malloc(sizeof(ride_t) * dataset.rides_number);

            if(rides)
            {
                rides_number_t i = 0;
                for (i = 0; i < dataset.rides_number; i++)
                {
                    if (parse_ride(dataset_file, &rides[i], &dataset) == -1)
                        break;
                }

                if(dataset.rides_number == i)
                {
                    dataset.rides = rides;
                    dataset.valid = true;

                    fclose(dataset_file);
                    return dataset;
                }

                syslog(LOG_ERR, "Failed to parse ride %"PRIrides_number, i);

                free(rides);
                fclose(dataset_file);
                return dataset;
            }

            syslog(LOG_ERR, "Failed to allocate rides memory");

            fclose(dataset_file);
            return dataset;
        }

        syslog(LOG_ERR, "Dataset header error");

        fclose(dataset_file);
        return dataset;
    }

    syslog(LOG_ERR, "Dataset file %s not found", path);
    return dataset;
}

void dataset_print(dataset_t *dataset, FILE *output)
{
    fprintf(output, "%"PRImap_coordinate" rows, %"PRImap_coordinate" columns, %"PRIvehicle_number" vehicles, %"PRIrides_number" rides, %"PRIbonus_size" bonus and %"PRIiteration_time" steps\n", dataset->rows, dataset->columns, dataset->vehicles_number, dataset->rides_number, dataset->bonus, dataset->steps);
    for (rides_number_t i = 0; i < dataset->rides_number; i++) {
        ride_t *ride = &dataset->rides[i];
        fprintf(output, "ride from [%"PRImap_coordinate", %"PRImap_coordinate"] to [%"PRImap_coordinate", %"PRImap_coordinate"], earliest start %"PRIiteration_time", latest finish %"PRIiteration_time"\n", ride->start.x, ride->start.y, ride->end.x, ride->end.y, ride->earliest_start, ride->latest_finish);
    }
}