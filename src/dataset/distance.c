//
// Created by Leonardo Medici on 2019-01-03.
//

#include <stdlib.h>
#include <syslog.h>

#include "distance.h"

map_distance_t manhattan(point_t origin, point_t destination)
{
    map_coordinate_t result = (map_coordinate_t) abs(destination.x - origin.x) + (map_coordinate_t) abs(destination.y - origin.y);
    syslog(LOG_DEBUG, "Manhattan distance between (%"PRImap_coordinate", %"PRImap_coordinate") and (%"PRImap_coordinate", %"PRImap_coordinate") is %"PRImap_distance, origin.x, origin.y, destination.x, destination.y, result);
    return result;
}