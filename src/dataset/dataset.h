//
// Created by Leonardo Medici on 2019-01-03.
//

#ifndef GOOGLE_HASH_CODE_2018_DATASET_H
#define GOOGLE_HASH_CODE_2018_DATASET_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

typedef uint16_t map_coordinate_t;
typedef uint16_t map_distance_t;
typedef uint32_t iteration_time_t;
typedef uint16_t vehicles_number_t;
typedef uint16_t rides_number_t;
typedef uint16_t bonus_size_t;

#define PRImap_coordinate PRIu16
#define PRImap_distance PRIu16
#define PRIiteration_time PRIu32
#define PRIvehicle_number PRIu16
#define PRIrides_number PRIu16
#define PRIbonus_size PRIu16

typedef struct point_s {
    map_coordinate_t x;
    map_coordinate_t y;
} point_t;

typedef struct ride_s {
    point_t start;
    point_t end;
    iteration_time_t earliest_start;
    iteration_time_t latest_finish;
} ride_t;

typedef struct dataset_s {
    bool valid;
    map_distance_t    (*distance)(point_t origin, point_t destination);
    map_coordinate_t  rows;
    map_coordinate_t  columns;
    vehicles_number_t vehicles_number;
    rides_number_t    rides_number;
    bonus_size_t      bonus;
    iteration_time_t  steps;
    ride_t *rides;
} dataset_t;

dataset_t dataset_load(char *path);
void dataset_print(dataset_t *dataset, FILE *output);

#endif //GOOGLE_HASH_CODE_2018_DATASET_H
