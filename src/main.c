#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>

#include "cli/cli.h"
#include "dataset/dataset.h"
#include "algorithm/algorithm.h"
#include "algorithm/common.h"

int getLogLevel(int verbosity)
{
    switch(verbosity)
    {
        case 1:
            return LOG_UPTO(LOG_NOTICE);
        case 2:
            return LOG_UPTO(LOG_INFO);
        case 3:
            return LOG_UPTO(LOG_DEBUG);
        default:
            return LOG_UPTO(LOG_ERR);
    }
}

int main(int argc, char **argv) {
    openlog(NULL, LOG_CONS | LOG_PID | LOG_NDELAY | LOG_PERROR, LOG_LOCAL1);
    setlogmask(LOG_UPTO (LOG_ERR));

    global_args_t args = parse_command_line(argc, argv);
    setlogmask(getLogLevel(args.verbosity));

    dataset_t dataset = dataset_load(args.input_file_name);

    if(dataset.valid)
    {
        if(args.print_dataset)
            dataset_print(&dataset, stdout);

        fflush(stdout);

        constructive_heuristic_t constructive_heuristic = parse_heuristic(args.constructive_heuristic);
        if(constructive_heuristic)
        {
            ART_config_t configuration = {
                    .dataset = &dataset,
                    .iterations = args.iterations,
                    .ban_time = args.ban_time,
                    .ban_probability = args.ban_probability,
                    .constructive_heuristic = constructive_heuristic,
                    .sort_criteria = parse_sort_criteria(args.sort_criteria),
                    .ban_criteria  = args.global_ban ? ban_rides_global : ban_rides_solution
            };

            solution_t *solution = ART_run(args.seed, configuration);

            if(solution)
            {
                FILE *output = stdout;
                if(args.output_file_name)
                {
                    FILE *output_file = fopen(args.output_file_name, "w");
                    if(!output_file)
                    {
                        output_file = stdout;
                        syslog(LOG_ERR, "Cannot write solution to %s fallback to stdout", args.output_file_name);
                    }

                    output = output_file;
                }

                solution_print(output, solution);
                fclose(output);
                solution_free(solution);
            }

            closelog();
            return EXIT_SUCCESS;
        }

        syslog(LOG_ERR, "Invalid constructive heuristic: %s", args.constructive_heuristic);
        closelog();
        display_usage();
        return EXIT_FAILURE;
    }

    syslog(LOG_ERR, "Error loading dataset file: %s", args.input_file_name);
    closelog();
    display_usage();
    return EXIT_FAILURE;
}